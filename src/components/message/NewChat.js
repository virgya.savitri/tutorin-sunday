import React, { useRef, useState , useEffect} from 'react';
import './newchat.css';
import SendIcon from '@material-ui/icons/Send';
import MicNoneIcon from '@material-ui/icons/MicNone';
import InputAdornment from '@material-ui/core/InputAdornment';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/analytics';
import user1 from "../../assets/user2.jpg";
import { useAuthState } from 'react-firebase-hooks/auth';
import { useCollectionData } from 'react-firebase-hooks/firestore';
import Countdown from "react-countdown";

firebase.initializeApp({
  apiKey: "AIzaSyA4ZUi6_kh5GhFzaxagkgq4uYzcTHYuDng",
  authDomain: "tutorin-317818.firebaseapp.com",
  databaseURL: "https://tutorin-317818-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "tutorin-317818",
  storageBucket: "tutorin-317818.appspot.com",
  messagingSenderId: "585176774732",
  appId: "1:585176774732:web:4a8f2ce035de2930c71ce9",
  // your config
})

const auth = firebase.auth();
const firestore = firebase.firestore();
const analytics = firebase.analytics();


function NewChat() {

  const [user] = useAuthState(auth);

  return (
    <div className="NewChat">
      <header>
        <img className="picture-chat" src={user1} alt="user1" />
        <h5 className="tutor-name"> Rezky Arishta</h5>
        <SignOut />
        
      </header>
      <div>
      <button className="more-button" type="submit" > <ArrowBackIosIcon />More</button>
      </div>
      
      <section>
        {user ? <ChatRoom /> : <SignIn />}
      </section>
     
    </div>
  );
}

function SignIn() {

  const signInWithGoogle = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider);
  }

  return (
    <>
      <br/>
      <button className="sign-in" onClick={signInWithGoogle}>Start the Session</button>
      <p>Welcome to Tutorin chat</p>
      {/* <Sidebar/> */}
    </>
  )

}

function SignOut() {
  return auth.currentUser && (
    <button className="count" onClick={() => auth.signOut()}>
      <Countdown className="count" date={Date.now() + 3600000} /></button>
  )
}


function ChatRoom() {
  const dummy = useRef();
  const messagesRef = firestore.collection('session-2');
  const query = messagesRef.orderBy('createdAt').limit(25);

  const [messages] = useCollectionData(query, { idField: 'id' });

  const [formValue, setFormValue] = useState('');


  const sendMessage = async (e) => {
    e.preventDefault();

    const { uid, photoURL } = auth.currentUser;

    await messagesRef.add({
      text: formValue,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      uid,
      photoURL
    })

    setFormValue('');
    dummy.current.scrollIntoView({ behavior: 'smooth' });
  }

  return (<>
    <div className="main-chat">
      {messages && messages.map(msg => <ChatMessage key={msg.id} message={msg} />)}
      <span ref={dummy}></span>
    </div>

    <form onSubmit={sendMessage}
    style={{
      height:"47px",
      position: "fixed",
      bottom: "140px",
      backgroundColor: "rgb(24, 23, 23)",
      borderRadius: "24px",
      border: "none",
      width: "60vw",
      maxWidth: "100vw",
      display: "flex",
      // fontSize: "14px",
      left: "25vw",
      }}>      
      <input className="input-chat"
        autocomplete="off"
        id="input-with-icon-adornment"
        startAdornment={
          <InputAdornment position="start">
            <MicNoneIcon />
          </InputAdornment>}
        className="input-message" 
        value={formValue} 
        onChange={(e) => setFormValue(e.target.value)} 
        placeholder="Type to message" />      
    </form>
    <button className="send-button" type="submit" disabled={!formValue}><SendIcon/></button>
  </>)
}


function ChatMessage(props) {
  const { text, uid, photoURL } = props.message;
  const messageClass = uid === auth.currentUser.uid ? 'sent' : 'received';
  return (<>
    <div className={`message ${messageClass}`}>
      <img className="imgchat" src={photoURL || 'https://api.adorable.io/avatars/23/abott@adorable.png'} />
      <p className="p-chat">{text}</p>
    </div>
  </>)
}

// const Sidebar = ({ width, height, children }) => {
//   const [xPosition, setX] = useState(-width);

//   const toggleMenu = () => {
//     if (xPosition > 0) {
//       setX(-width);
//     } else {
//       setX(0);
//     }
//   };

//   useEffect(() => {
//     setX(0);
//   }, []);
//   return (
//     <React.Fragment>
//       <div
//         className="side-bar-chat"
//         style={{
//           transform: `translatex(${xPosition}px)`,
//           width: width,
//           minHeight: height
//         }}
//       >
//         <button
//           onClick={() => toggleMenu()}
//           className="toggle-menu-chat"
//           style={{
//             transform: `translate(${width}px, 20vh)`
//           }}
//         ></button>
//         <div className="content">{children}</div>
//       </div>
//     </React.Fragment>
//   );
// }

export default NewChat;