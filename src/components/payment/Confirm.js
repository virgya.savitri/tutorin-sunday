import react, {useState, useEffect} from 'react';
import axios from 'axios'
import "./payment.css";
import Modal from "react-modal";
import ReactDOM from 'react-dom';
import { useHistory } from "react-router-dom";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

Modal.setAppElement("#root");

function Confirm (){
    const [paymentDetails, setPaymentDetails] = useState([]);
    const [selectedFile, setSelectedFile] = useState("");
    const [loginModal, setLoginModal] = useState(true);

    const [modalIsOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    const history = useHistory();
  
    function handleClick() {
      history.push("./message");
    }
    
    function handleClick1() {
        history.push("./dashboard");
      }

  useEffect(() => {
    const store = window.localStorage;
    const token = store.getItem("token");
    axios
      .get("https://tutorins.herokuapp.com/api/v1/booking/student", {
        headers: {
          Authorization: token,
        },
      })
      .then((response) => {
        console.log(response.data.data);
        setPaymentDetails(response.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const [databooking, setDatabooking] = useState({
    price: 0,
    date: "",
    start_time: "",
    end_time: "",
    duration: 0,
    payment_method: "",
  });
  function handle(e) {
    const newdatabooking = { ...databooking };
    newdatabooking[e.target.id] = e.target.value;
    setDatabooking(newdatabooking);
  }

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined);
      return;
    }
    setSelectedFile(e.target.files[0]);
  };

  useEffect(() => {
    const store = window.localStorage;
    const token = store.getItem("token");
    axios
      .get("https://tutorins.herokuapp.com/api/v1/booking/student", {
        headers: {
          Authorization: token,
        },
      })
      .then((response) => {
        console.log(response.data.data);
        setPaymentDetails(response.data.data);
        console.log(response.data.data)
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="main-confirm">
      <h2> Payment Confirmation</h2>

      <div>
        {/* <h4> Data Booking </h4>
        <h5> Student Name : Melinda Nexy</h5>
        <br/> */}
        <div>
          <p>Payment Method</p>
            <select
              value={databooking.payment_method}
              id="payment_method"
              onChange={(e) => handle(e)}
              className="dropdssss"
            >
              <option value="none" hidden>
                Please Select Payment Method...
              </option>
              <option value="Bank Transfer">Bank Transfer</option>
              <option value="Debit/Credit Card">Debit/Credit Card</option>
              <option value="E Wallet">E Wallet</option>
            </select>
          </div>
          <div>
            <br/>
          <p>Bank Account</p>
            <select
              value={databooking.payment_method}
              id="payment_method"
              onChange={(e) => handle(e)}
              className="dropdssss"
            >
              <option value="none1" hidden>
                Bank Account
              </option>
              <option value="Bank Transfer">BCA</option>
              <option value="Debit/Credit Card">BRI</option>
             
            </select>
          </div>
          <br/>

          <div className="sender">
            <p>Account Name</p>
            <input 
              className="sender"
              placeholder="Type the account name"      
              style={{
                width: "755px",
                height: "40px",
                padding: "10px",
              }}
              /> 
          </div>
          <br/>

          <div className="sender">
            <p>Email</p>
            <input 
              className="sender"
              placeholder="Email"      
              style={{
                width: "755px",
                height: "40px",
                padding: "10px",
              }}
              /> 
          </div>
          <br/>
          
          <div className="upload-sender">
            <p className="para">Transfer Receipt</p>
            <input className="input-b"onChange={onSelectFile} type="file"></input>
        </div>
        <br/>
        <button 
            onClick={openModal}
        className="submit-b">Submit Information</button>
      </div>
              

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        // style={customStyles}
      >
        {/* {paymentDetails.map((e) => { */}
        <div>
          <div className="info-3">
            <h3> Payment Receipt</h3>
          </div>
            <br/>
            <div className="info-c">
            <p className="info-1"> Payment for :</p>
            <p className="info-1"> Rezky Arishta</p>
            <p className="info-1"> Price Rp 1.000.000,-</p>
            <p className="info-1"> Schedule on 24 July 2021, 1 PM - 2 PM  </p>
          </div>
          <br/>
          <div className="info-d">
            <p className="info-2"> Transfer from : </p>
            <p className="info-2"> - BANK BCA</p>
            <p className="info-2"> - Sender Name : Melinda Nexy</p>
            <p className="info-2"> - Receipt Submitted</p>
            <p></p>
          </div>
          <br/>
          <div className="info-e">
          Your information has been submitted, Please wait for the confirmation
          </div>
          <br/>
          <button className="info-f" onClick={handleClick}>
           open the Chatroom
          </button>
          <button className="info-f" onClick={handleClick1}>
           go to Dashboard
          </button>
        </div>

       
      </Modal>

    </div>

  )
}

export default Confirm